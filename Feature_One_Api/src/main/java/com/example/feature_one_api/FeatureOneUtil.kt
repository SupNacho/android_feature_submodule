package com.example.feature_one_api

import com.example.feature_one_api.model.FeatureOneEvent

interface FeatureOneUtil {

    fun getSomeFeatureAction(event: FeatureOneEvent): String
}