package com.example.feature_one_api.model

sealed interface FeatureOneEvent {
    object Greetings: FeatureOneEvent
    object Farewell: FeatureOneEvent
    object RequestGuestName: FeatureOneEvent
    object NotifyMasters: FeatureOneEvent
    object FireTheButler: FeatureOneEvent
    object HireButler: FeatureOneEvent
}