package com.example.feature_one_impl

import com.example.feature_one_api.FeatureOneUtil
import com.example.feature_one_api.model.FeatureOneEvent


class FeatureOneUtilImpl: FeatureOneUtil {
//
    override fun getSomeFeatureAction(event: FeatureOneEvent): String {
        return when(event) {
            FeatureOneEvent.Farewell -> "Farewell"
            FeatureOneEvent.Greetings -> "Hello!"
            FeatureOneEvent.NotifyMasters -> "Masters, guests arrived!"
            FeatureOneEvent.RequestGuestName -> "What is your name?"
            FeatureOneEvent.FireTheButler -> "Fire in hell, bastards!"
            FeatureOneEvent.HireButler -> "Welcome aboard"
        }
    }
}